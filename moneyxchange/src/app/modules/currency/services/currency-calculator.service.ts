import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CurrencyRequest } from '../models/currency-request';


@Injectable({
  providedIn: 'root'
})
export class CurrencyCalculatorService {

  constructor(private http: HttpClient) {
  }

  public calculate(request: CurrencyRequest): Observable<any> {
    return this.http.get('http://localhost:9001/api/forex?currency=DOLLAR');
  }
}
