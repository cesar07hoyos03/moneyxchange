import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyInputComponent } from './components/currency-input/currency-input.component';
import { CurrencyButtonComponent } from './components/currency-button/currency-button.component';
import { CurrencyContainerComponent } from './components/currency-container/currency-container.component';
import { MaterialModule } from '../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyInputDirective } from './components/currency-input/currency-input.directive';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    CurrencyInputComponent,
    CurrencyButtonComponent,
    CurrencyContainerComponent,
    CurrencyInputDirective
  ],
  exports: [CurrencyContainerComponent]
})
export class CurrencyModule {
}
