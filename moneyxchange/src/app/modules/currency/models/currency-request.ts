export interface CurrencyRequest {
  currencyOrigin: any;
  currencyDestination: any;
  value: number;
}
