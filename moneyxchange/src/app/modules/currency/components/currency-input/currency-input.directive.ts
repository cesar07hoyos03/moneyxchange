import { Directive, HostListener, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common';

export const CURRENCY_IDENTIFIER = new Map([
  ['USD', '$'],
  ['EUR', '€']
]);

@Directive({
  selector: '[appCurrency]',
  providers: [DecimalPipe]
})
export class CurrencyInputDirective {

  @Input()
  currencyType = '';

  constructor(private decimalPipe: DecimalPipe) {
  }

  @HostListener('input', ['$event'])
  @HostListener('blur', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;

    let trimmed = input.value.replace(/\s+/g, ''); // spaces
    trimmed = trimmed.replace(/[^0-9.]/g, '') || '';

    if (!trimmed) {
      trimmed = '0';
    }

    if (trimmed.length > 10) {
      trimmed = trimmed.substr(0, 10);
    }

    const symbol = CURRENCY_IDENTIFIER.get(this.currencyType) || '';

    if (trimmed.endsWith('.')) {
      input.value = symbol + '' + trimmed;
    } else {
      input.value = symbol + '' + this.decimalPipe.transform(trimmed, '1.0-2');
    }
  }

}
