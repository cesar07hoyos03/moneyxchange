import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss']
})
export class CurrencyInputComponent implements OnInit {

  @Input()
  currencyType: string;

  @Input()
  isReadOnly = false;

  amount;

  constructor() {
  }

  ngOnInit() {
  }

  public getCurrencyValue(): number {
    if (this.amount && this.amount.length > 0) {
      return Number(this.amount.replace(/[^0-9.]/g, ''));
    }

    return 0;
  }

}
