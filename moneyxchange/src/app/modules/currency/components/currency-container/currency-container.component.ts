import { Component, OnInit, ViewChildren } from '@angular/core';
import { CurrencyInputComponent } from '../currency-input/currency-input.component';
import { CURRENCY_IDENTIFIER } from '../currency-input/currency-input.directive';
import { CurrencyCalculatorService } from '../../services/currency-calculator.service';
import { CurrencyRequest } from '../../models/currency-request';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-currency-container',
  templateUrl: './currency-container.component.html',
  styleUrls: ['./currency-container.component.scss'],
  providers: [CurrencyCalculatorService]
})
export class CurrencyContainerComponent implements OnInit {

  @ViewChildren(CurrencyInputComponent)
  currencies;

  isLoading = false;

  constructor(private currencyCalculatorService: CurrencyCalculatorService) {
  }

  ngOnInit() {
  }

  public calculate(): void {
    const [firstCurrency, secondCurrency] = this.currencies.toArray();
    const currencyRequest: CurrencyRequest = {
      value: firstCurrency.getCurrencyValue(),
      currencyOrigin: firstCurrency.currencyType,
      currencyDestination: secondCurrency.currencyType
    };


    this.isLoading = true;
    this.currencyCalculatorService.calculate(currencyRequest)
      .subscribe((response) => {
        console.log(response);
        setTimeout(() => {
          secondCurrency.amount = CURRENCY_IDENTIFIER.get('EUR') + (currencyRequest.value * response.value);
          this.isLoading = false;
        }, 500);
      }, (error: HttpErrorResponse) => {
        this.isLoading = false;
      });
  }
}
