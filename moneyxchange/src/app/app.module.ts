import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HomeComponent } from './components/home/home.component';
import { SharedModule } from './modules/shared/shared.module';
import { CurrencyModule } from './modules/currency/currency.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    SharedModule,
    CurrencyModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class AppModule {
}
