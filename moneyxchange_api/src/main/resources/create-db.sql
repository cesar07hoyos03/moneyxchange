create schema moneyxchange;

create table moneyxchange.forex(
  id varchar(38) primary key,
  currency varchar(10),
  date date,
  value numeric
)