/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package io.moneyxchange.api.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/**
 * BaseEntity
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@MappedSuperclass
@Data
public abstract class BaseEntity {

    /**
     * id
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected UUID id;
}




