/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package io.moneyxchange.api.model.entity;

import io.moneyxchange.api.constant.Currency;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Forex Entity
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "forex", schema = "moneyxchange")
public class Forex extends BaseEntity {

    /**
     * Forex year
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Currency currency;

    /**
     * Forex ending date
     */
    @Column
    @Temporal(TemporalType.DATE)
    private Date date;

    /**
     * Forex rate value
     */
    @Column
    private Double value;

}
