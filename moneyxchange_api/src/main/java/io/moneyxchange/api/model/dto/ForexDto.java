/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package io.moneyxchange.api.model.dto;

import io.moneyxchange.api.constant.Currency;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * ForexDto
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ForexDto implements Serializable {

    /**
     * Forex currency
     */
    private Currency currency;

    /**
     * Forex date
     */
    private Date date;

    /**
     * Forex value
     */
    private Double value;

}
