/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package io.moneyxchange.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * Moneyxchange Api Spring boot application
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 */
@EnableWebSecurity
@SpringBootApplication
@EnableScheduling
public class MoneyxchangeApiApplication {

    /**
     * Main Application
     *
     * @param args - console arguments
     * @since 0.0.1
     */
    public static void main(String[] args) {
        SpringApplication.run(MoneyxchangeApiApplication.class, args);
    }

}
