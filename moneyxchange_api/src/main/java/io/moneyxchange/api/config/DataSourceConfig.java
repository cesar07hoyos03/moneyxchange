/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package io.moneyxchange.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Moneyxchange Datasource configuration
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("io.moneyxchange.api.repository")
public class DataSourceConfig {


    /**
     * Datasource configuration
     *
     * @return HikariDataSource connection pool configured
     */
    @Bean
    public DataSource dataSource() {

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("create-db.sql")
                .build();
        return db;
    }

}
