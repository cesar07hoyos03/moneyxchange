/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package io.moneyxchange.api.controller;

import io.moneyxchange.api.constant.Currency;
import io.moneyxchange.api.constant.ResourceEndpoint;
import io.moneyxchange.api.mapper.ForexMapper;
import io.moneyxchange.api.model.dto.ForexDto;
import io.moneyxchange.api.model.entity.Forex;
import io.moneyxchange.api.service.api.IForexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Controller to receive Forex requests
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Slf4j
@RestController
@RequestMapping(value = ResourceEndpoint.FOREX_RESOURCE)
public class ForexController {

    /**
     *
     */
    private IForexService forexService;

    private ForexMapper mapper;

    /**
     * @param forexService
     */
    @Autowired
    public ForexController(IForexService forexService, ForexMapper mapper) {
        this.forexService = forexService;
        this.mapper = mapper;
    }

    /**
     * saveMeasurementForMonth
     */
    @GetMapping
    ResponseEntity<ForexDto> getExchangeValue(@Valid @NotNull @RequestParam("currency") Currency currency) {

        log.info("Received request for get exchange value");
        ForexDto response = mapper.forexToForexDto(this.forexService.findForeignExchange(currency));
        log.info(response.toString());
        return ResponseEntity.ok().body(response);
    }



}
