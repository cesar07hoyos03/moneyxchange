/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package io.moneyxchange.api.mapper;

import io.moneyxchange.api.model.dto.ForexDto;
import io.moneyxchange.api.model.entity.Forex;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * ForexMapper
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Mapper(componentModel = "spring")
public interface ForexMapper {

    /**
     * INSTANCE
     */
    ForexMapper INSTANCE = Mappers.getMapper(ForexMapper.class);

    /**
     * @param source
     * @return ForexDto
     */
    Forex forexDtoToForex(ForexDto source);

    /**
     * Inverse mapping
     *
     * @param forex
     * @return
     */
    @InheritInverseConfiguration
    ForexDto forexToForexDto(Forex forex);

}
