/*
 * (C) Copyright 2018 and others.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package io.moneyxchange.api.service.impl;

import io.moneyxchange.api.constant.Currency;
import io.moneyxchange.api.model.entity.Forex;
import io.moneyxchange.api.repository.ForexRepository;
import io.moneyxchange.api.service.api.IForexService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * ForexService
 *
 * @author <a href="cesar07hoyos03@gmail.com">C&eacute;sar Hoyos</a>
 * @since 0.0.1
 */
@Log
@Service
public class ForexService implements IForexService {

    /**
     * forex Repository
     */
    @Autowired
    private ForexRepository forexRepository;

    /**
     * @return Forex
     * @param currency
     */
    @Override
    public Forex findForeignExchange(Currency currency) {
        log.info(String
                .format("Finding last forex rate value"));

        return forexRepository.findFirstByCurrencyOrderByDateDesc(currency).orElse(
                forexRepository.save(
                        Forex.builder()
                                .currency(currency)
                                .date(new Date())
                                .value(Math.random()).build())
        );
    }


}
